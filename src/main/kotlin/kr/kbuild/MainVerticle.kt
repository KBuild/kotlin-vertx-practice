package kr.kbuild

import io.vertx.core.AbstractVerticle
import io.vertx.core.Future
import io.vertx.ext.web.Router
import io.vertx.kotlin.core.http.HttpServerOptions

@VerticleClass class MainVerticle : AbstractVerticle() {
    private val router = Router.router(vertx).apply {
        get("/:magic").handler { ctx ->
            ctx.response.end("Hello ${ctx.request.getParam("magic")}!")
        }
    }

    override fun start(startFuture: Future<Void>?) {
        vertx.createHttpServer(HttpServerOptions(
                port = Integer.getInteger("http.port", 9000)
        )).requestHandler {
            router.accept(it)
        }.listen { result ->
            if (result.succeeded()) {
                startFuture?.complete()
            } else {
                startFuture?.fail(result.cause())
            }
        }
    }
}

